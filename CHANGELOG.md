# 26.06.23
- Définition du projet

---
# 04.07.23
- Création de la nomenclature des fichiers et dossiers
- Ajout du bouton thème clair/sombre
- Ajout robots.txt
- Début codage html/css

---
# 05.08.23
- Mise en place de la recréation du site

---
# 15.08.23
- Ajout du jeu "Canasta"
- Création du style pour les tableaux
- Correction du chemin vers humans.txt dans jeux

---
# 09/11/24
- Ajout du jeu "Tamalo"
- Correction liens et images

---
# 13/11/24
- Ajout "Kem's"

---
# 16/11/24
- Ajout "Menteur"

---
# 15/01/25
- Ajout "Rami"

---
# 18/01/25
- Ajout "Pouilleux"
- Ajout "Palmier"

---
# 29/01/25
- Correction lien Menteur
- Ajout encart test sur "Tamalou"

---
# 05/02/25
- Ajout "Bridge"
- Rangement ordre alphabétique
- Correction mise en page

---
# 06/02/25
- Ajout "Tarot"

---
# 13/02/35
- Ajout "Belote"

---
# 14/02/25
- Ajoute "Brisque"
- Correction duplkicat codes

---
# 16/02/25
- Ajout "Piquet"
- Suppression "Manille" (trop similaire aux autres jeux de levées)

---
# 18/02/25
- Ajout "Polignac"

---
# 19/02/25
- Ajout "Skat"