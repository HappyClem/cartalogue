L'initiative “Humans.txt” vise à présenter brièvement les personnes impliquées dans la création et la maintenance d'un site web.
Plus d'informations : https://humanstxt.org/FR
Reconnaître les humains derrière le web.

/* TEAM */
Clémentin Sauvion
France                         
							
/* THANKS */

/* SITE */
Last update:  2025/02/14         
Standards: HTML5, CSS3                 
Software: VSCodium (code), Inkscape (logo)
Font : Geologica (https://fonts.google.com/specimen/Geologica)


                    